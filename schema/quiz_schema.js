const { gql } = require('apollo-server-express');

const quizTypeDefs = gql`
	type Question {
		type: String
		title: String
		answers: [String!]
		correctAnswers: [String!]
	}

	type Quiz @key(fields: "_id") {
		_id: String!
		title: String,
    	questions: [Question!],
    	sequence: Int,
	}

	input QuestionInput {
		type: String!
		title: String!
		answers: [String!]
		correctAnswers: [String!]
	}

	input QuizInput {
		_id: String,
		title: String!,
    	questions: [QuestionInput!],
    	sequence: Int!
	}
	
	type Query {
		getQuiz: [Quiz!]
	}

	type Mutation {
		mutationQuiz(input: QuizInput!): Quiz!
	}
`

module.exports = quizTypeDefs;