const { gql } = require('apollo-server-express');

const topicTypeDefs = gql`
	type Topic @key(fields: "_id") {
		_id: String!
		slug: String
		name: String
		parent: Topic
	}

	input TopicInput {
		_id: String
		name: String!
		parent: String
	}
	
	type Query {
		getTopics: [Topic!]
	}

	type Mutation {
		mutationTopic(input: TopicInput!): Topic!
	}
`

module.exports = topicTypeDefs;