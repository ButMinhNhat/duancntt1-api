const { gql } = require('apollo-server-express');

const courseTypeDefs = gql`
	extend type Account @key(fields: "_id") {
		_id: String!
	}

	extend type Quiz @key(fields: "_id") {
		_id: String! 
	}
	
	extend type Topic @key(fields: "_id") {
		_id: String! @external
		name: String
	}

	type Lecture {
		_id: String
		title: String
        media: String
        files: [String!]
        content: String
        sequence: Int
	}

	type CourseSection {
		title: String
    	quiz:  [Quiz]
    	lectures: [Lecture]
	}

	type Course @key(fields: "_id") {
		_id             : String
		title           : String
		slug            : String
		status          : String
		attendees		: [Account!]
		sections        : [CourseSection]
		shortDescription: String
		description     : String
		coverImage      : String
		level           : String
		hashtags        : [String!]
		features		: [String!]
		topic           : [Topic]
		createdBy       : Account
		createdAt       : String
		updatedAt       : String
	}

	input LectureInput {
		_id: String
		title: String!
        media: String
        files: [String!]
        content: String
        sequence: Int
	}

	input CourseSectionInput {
		title: String!
    	quiz:  [String]
    	lectures: [LectureInput!]
	}

	input CourseInput {
		_id				: String
		title           : String!
		status          : String
		attendees		: [String!]
		sections        : [CourseSectionInput!]
		shortDescription: String
		description     : String
		coverImage      : String
		level           : String
		hashtags        : [String!]
		features        : [String!]
		topic           : [String!]
	}
	
	type Query {
		getCourses(search: String, topic: [String!], limit: Int): [Course!]
		getCourseBySlug(slug: String!): Course! 
		getInstructorCourses: [Course!]
		getInstructorCourseBySlug(slug: String!): Course!
		getMyLearningCourses: [Course!]
	}

	type Mutation {
		mutationCourse(input: CourseInput!): Course!
	}
`

module.exports = courseTypeDefs;