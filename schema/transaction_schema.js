const { gql } = require('apollo-server-express');

const transactionTypeDefs = gql`
	extend type Account @key(fields: "_id") {
		_id: String!
	}

	extend type Course @key(fields: "_id") {
		_id: String!
	}

	type Answer {
		text	: String
		chooses	: [String!]
	}

	type Content {
		value	: Float
		text	: String
		answers	: [Answer!]
		score	: Float
	}

	type Transaction @key(fields: "_id") {
		_id		: String!
		type	: String
		course	: Course
		parent	: String
		content : Content!
		createdBy: Account
		createdAt: String
		updatedAt: String
	}

	input AnswerInput {
		text	: String
		chooses	: [String!]
	}

	input ContentInput {
		value	: Float
		text	: String
		answers	: [AnswerInput!]
		score	: Float
	}

	input TransactionInput {
		type	: String!
		course	: String!
		parent	: String
		content : ContentInput
	}

	type Query {
		queryTransactionsByType(types: [String!], courseId: String!): [Transaction!]
	}

	type Mutation {
		mutationTransaction(input: TransactionInput!): Transaction!
	}
`

module.exports = transactionTypeDefs;