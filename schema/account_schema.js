const { gql } = require('apollo-server-express');

const accountTypeDefs = gql`
	type Account @key(fields: "_id") {
		_id: String!
		name: String
		username: String!
		password: String!
		avatar: String
	}

	type LoginAccount {
		data: Account!
		token: String!
	}

	input RegisterInput {
		name: String
		username: String!
		password: String!
		avatar: String
	}

	input LoginInput {
		username: String!
		password: String!
	}
	
	type Query {
		queryAccount: [Account!]
	}

	type Mutation {
		registerAccount(input: RegisterInput!): LoginAccount!
		loginAccount(input: LoginInput!): LoginAccount!
	}
`

module.exports = accountTypeDefs;