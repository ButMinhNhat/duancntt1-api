const { gql } = require('apollo-server-express');

const trackingTypeDefs = gql`
	extend type Account @key(fields: "_id") {
		_id: String!
	}

	extend type Course @key(fields: "_id") {
		_id: String!
	}

	type Tracking @key(fields: "_id") {
		_id		: String!
		type	: String
		course	: Course
		data	: [String!]
		createdBy: Account
		createdAt: String
		updatedAt: String
	}

	input TrackingInput {
		_id		: String
		type	: String
		course	: String
		data	: [String!]
	}

	type Query {
		queryTrackingForCourses(type: String!, courseIds: [String!]): [Tracking!]
		queryTracking(type: String!, courseId: String!): Tracking
	}

	type Mutation {
		mutationTracking(input: TrackingInput!): Tracking!
	}
`

module.exports = trackingTypeDefs;