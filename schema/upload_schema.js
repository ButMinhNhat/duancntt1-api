const { gql } = require('apollo-server-express');

const uploadTypeDefs = gql`
	scalar Upload

	type Mutation {
		uploadFile(file: Upload!): String!
	}
`

module.exports = uploadTypeDefs;