const Topic = require("../models/topic_model");
const httpStatus = require("http-status");
const ApiError = require("../utils/ApiError");
const { slugGenerator } = require("../utils/slug_generator");

const resolvers = {
  Topic: {
    __resolveReference: async (item) => Topic.findById(item._id).exec(),
  },

  //Query
  Query: {
    getTopics: async () => Topic.find().populate("parent").exec(),
  },

  // MUTATION
  Mutation: {
    mutationTopic: async (_, { input }) => {
      try {
        const { _id } = input;

        if (_id) {
          const result = await Topic.findByIdAndUpdate(_id, {
			...input,
			slug: slugGenerator(input.name)
		  }, {
            new: true,
          });
          return result;
        } else {
          const result = await Topic.create({
			...input,
			slug: slugGenerator(input.name)
		  });
          return result;
        }
      } catch (error) {
        console.log(error);
        throw new ApiError(
          httpStatus.INTERNAL_SERVER_ERROR,
          "Save topic fail!"
        );
      }
    },
  },
};

module.exports = resolvers;
