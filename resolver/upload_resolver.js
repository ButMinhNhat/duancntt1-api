const { GraphQLUpload } = require("graphql-upload");
const mongoose = require('mongoose');
const Grid = require('gridfs-stream');
const fs = require('fs');

const storeFile = async (upload) => {
  const { filename, createReadStream, mimetype } = await upload.then(
    (result) => result
  );

  const bucket = new mongoose.mongo.GridFSBucket(mongoose.connection.db, {
    bucketName: "files",
  });

  const uploadStream = bucket.openUploadStream(`${Date.now()}-${filename.replace(/ /g, '-')}`, {
    contentType: mimetype
  });
  return new Promise((resolve, reject) => {
    createReadStream()
      .pipe(uploadStream)
      .on("error", reject)
      .on("finish", () => {
        resolve(uploadStream.filename);
      });
  });
};

const resolvers = {
  Upload: GraphQLUpload,

  Mutation: {
    uploadFile: async (_, { file }) => {
      const fileName = await storeFile(file).then((result) => result);

      return fileName;
    },
  },
};

module.exports = resolvers;
