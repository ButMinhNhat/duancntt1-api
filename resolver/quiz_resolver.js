const Quiz = require("../models/quiz_model");
const httpStatus = require("http-status");
const ApiError = require("../utils/ApiError");

const resolvers = {
  //Query
  Query: {
    getQuiz: async () => Quiz.find().exec(),
  },

  // MUTATION
  Mutation: {
    mutationQuiz: async (_, { input }) => {
      try {
        const { _id } = input;

        if (_id) {
          const result = await Quiz.findByIdAndUpdate(_id, input, {
            new: true,
          });
          return result;
        } else {
          const result = await Quiz.create(input);
          return result;
        }
      } catch (error) {
        console.log(error);
        throw new ApiError(
          httpStatus.INTERNAL_SERVER_ERROR,
          "Save quiz fail!"
        );
      }
    },
  },
};

module.exports = resolvers;
