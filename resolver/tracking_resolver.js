const Tracking = require("../models/tracking_model");
const httpStatus = require("http-status");
const ApiError = require("../utils/ApiError");

const resolvers = {
  //Query
  Query: {
	queryTrackingForCourses: async (_, { type, courseIds }, { userId }) => {
	  if(!userId) throw new ApiError(httpStatus.UNAUTHORIZED, "Unauthorized");

	  const filter = {
		type: type,
		createdBy: userId
	  };
	  if(courseIds) {
		filter.course = { $in: courseIds }
	  }

	  const result = await Tracking
	  				.find(filter)
					.populate("createdBy")
					.populate("course");

	  return result;
	},
    queryTracking: async (_, { type, courseId }, { userId }) => {
	  if(!userId) throw new ApiError(httpStatus.UNAUTHORIZED, "Unauthorized");

      const filter = {
		type: type,
        course: courseId,
		createdBy: userId
      };

      const result = await Tracking.findOne(filter).populate("createdBy");

      return result;
    },
  },

  // MUTATION
  Mutation: {
    mutationTracking: async (_, { input }, { userId }) => {
      try {
        const { _id } = input;
        if (!userId)
          throw new ApiError(httpStatus.UNAUTHORIZED, "Unauthorized");
        if (_id) {
          const result = await Tracking.findByIdAndUpdate(_id, input, {
            new: true,
          });
          return result;
        } else {
          const result = await Tracking.create({
            ...input,
            createdBy: userId,
          });
          return result;
        }
      } catch (error) {
        console.log(error);
        throw new ApiError(
          httpStatus.INTERNAL_SERVER_ERROR,
          "Save tracking fail!"
        );
      }
    },
  },
};

module.exports = resolvers;
