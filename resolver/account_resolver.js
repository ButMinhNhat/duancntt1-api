const Account = require("../models/account_model");
const httpStatus = require("http-status");
const ApiError = require("../utils/ApiError");
const bcrypt = require("bcrypt");
const { generateAccessToken } = require("../utils/api_token");

const salt = 10;

const resolvers = {
  //Query
  Query: {
    queryAccount: async () => Account.find().exec(),
  },

  // MUTATION
  Mutation: {
    registerAccount: async (_, { input }) => {
      try {
        const { username } = input;
        const isExistAccount = await Account.findOne({ username: username });

        if (isExistAccount) {
          return new ApiError(
            httpStatus.BAD_REQUEST,
            "Username is already taken"
          );
        }

        const result = await Account.create({
          ...input,
          password: bcrypt.hashSync(input.password, salt),
        });
        if (result) {
          return {
            data: result,
            token: generateAccessToken({ userId: result._id }),
          };
        }
      } catch (error) {
        console.log(error);
      }
      return new ApiError(httpStatus.INTERNAL_SERVER_ERROR, "Error occur");
    },
    loginAccount: async (_, { input }) => {
      try {
        const { username, password } = input;

        if (!username || !password) {
          return new ApiError(
            httpStatus.INTERNAL_SERVER_ERROR,
            "Username or password is required"
          );
        }

        const result = await Account.findOne({ username: username });

        if (!result || !bcrypt.compareSync(password, result.password)) {
          return new ApiError(
            httpStatus.INTERNAL_SERVER_ERROR,
            "Invalid username or password. Please input valid username and password!"
          );
        }

        return {
          data: result,
          token: generateAccessToken({ userId: result._id }),
        };
      } catch (error) {
        console.log(error);
      }
      return new ApiError(httpStatus.INTERNAL_SERVER_ERROR, "Error occur");
    },
  },
};

module.exports = resolvers;
