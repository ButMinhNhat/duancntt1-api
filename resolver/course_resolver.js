const Course = require("../models/course_model");
const httpStatus = require("http-status");
const ApiError = require("../utils/ApiError");
const Topic = require("../models/topic_model");

const slugGenerator = (str) => {
  str = str.replace(/^\s+|\s+$/g, ""); // trim
  str = str.toLowerCase();

  // remove accents, swap ñ for n, etc
  const from =
    "àáãảạăằắẳẵặâầấẩẫậèéẻẽẹêềếểễệđùúủũụưừứửữựòóỏõọôồốổỗộơờớởỡợìíỉĩịäëïîöüûñçýỳỹỵỷ";
  const to =
    "aaaaaaaaaaaaaaaaaeeeeeeeeeeeduuuuuuuuuuuoooooooooooooooooiiiiiaeiiouuncyyyyy";
  for (var i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
  }

  str = str
    .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
    .replace(/\s+/g, "-") // collapse whitespace and replace by -
    .replace(/-+/g, "-"); // collapse dashes

  return str;
};

const resolvers = {
  Topic: {
    __resolveReference: async (item) => Topic.findById(item._id).exec(),
  },

  //Query
  Query: {
    getCourses: async (_, { search, topic, limit }) => {
      const filter = {
        status: "Public"
      };
	  if(search) {
		filter.slug = { $regex : slugGenerator(search) }
	  };
	  if(topic) {
		filter.topic = { $in: topic }
	  }

      const result = Course.find(filter)
	  	.sort({ createdAt: -1 })
		.limit(limit ?? 20)
        .populate("attendees")
        .populate("topic")
        .populate("sections.quiz")
        .populate("createdBy");

      return result;
    },
    getCourseBySlug: async (_, { slug }, { userId }) => {
      const or = [
        {
          slug: slug,
          status: "Public",
        },
      ];
      if (userId) {
        or.push({
          slug: slug,
          status: "Shared",
          attendees: { $in: [userId] },
        });
      }
      const filter = {
        $or: or,
      };

      const result = await Course.findOne(filter)
        .populate("attendees")
        .populate("topic")
        .populate("sections.quiz")
        .populate("createdBy");

      if (!result) {
        throw new ApiError(httpStatus.NOT_FOUND, "Course not found!");
      }

      return result;
    },
    getInstructorCourses: async (_, {}, { userId }) =>
      Course.find({ createdBy: userId })
        .populate("attendees")
        .populate("topic")
        .populate("sections.quiz")
        .populate("createdBy")
        .sort({ createdAt: -1 })
        .exec(),
    getInstructorCourseBySlug: async (_, { slug }, { userId }) => {
      const result = await Course.findOne({ slug: slug, createdBy: userId })
        .populate("attendees")
        .populate("topic")
        .populate("sections.quiz")
        .populate("createdBy");

      if (!result) {
        throw new ApiError(httpStatus.NOT_FOUND, "Course not found!");
      }

      return result;
    },
    getMyLearningCourses: async (_, {}, { userId }) => {
      const filter = {
        status: { $in: ["Public", "Shared"] },
        attendees: { $in: [userId] },
      };

      const result = Course.find(filter)
        .populate("attendees")
        .populate("topic")
        .populate("sections.quiz")
        .populate("createdBy")
        .sort({ createdAt: -1 });

      return result;
    },
  },

  // MUTATION
  Mutation: {
    mutationCourse: async (_, { input }, { userId }) => {
      try {
        if (!userId)
          throw new ApiError(httpStatus.UNAUTHORIZED, "Unauthorized");

        const { _id } = input;

        if (_id) {
          const result = await Course.findByIdAndUpdate(
            _id,
            {
              ...input,
              slug: slugGenerator(input.title),
            },
            {
              new: true,
            }
          );
          return result;
        } else {
          const result = await Course.create({
            ...input,
            slug: slugGenerator(input.title),
            createdBy: userId,
          });
          return result;
        }
      } catch (error) {
        console.log(error);
        throw new ApiError(
          httpStatus.INTERNAL_SERVER_ERROR,
          "Save course fail!"
        );
      }
    },
  },
};

module.exports = resolvers;
