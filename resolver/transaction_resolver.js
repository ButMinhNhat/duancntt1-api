const Transaction = require("../models/transaction_model");
const httpStatus = require("http-status");
const ApiError = require("../utils/ApiError");

const resolvers = {
  //Query
  Query: {
    queryTransactionsByType: async (_, { types, courseId }, { userId }) => {
	  const filter = {
		course: courseId,
	  };
	  if(types) {
		filter.type = { $in: types }
	  }

      const result = await Transaction.find(filter).populate("createdBy");

	  if (!result) {
        throw new ApiError(httpStatus.NOT_FOUND, "Transaction not found!");
      }

      return result;
	}
  },

  // MUTATION
  Mutation: {
    mutationTransaction: async (_, { input }, { userId }) => {
      try {
        const { _id } = input;
        if (!userId)
          throw new ApiError(httpStatus.UNAUTHORIZED, "Unauthorized");
        if (_id) {
          const result = await Transaction.findByIdAndUpdate(_id, input, {
            new: true,
          });
          return result;
        } else {
          const result = await Transaction.create({
			...input,
			createdBy: userId,
		  });
          return result;
        }
      } catch (error) {
        console.log(error);
        throw new ApiError(
          httpStatus.INTERNAL_SERVER_ERROR,
          "Save transaction fail!"
        );
      }
    },
  },
};

module.exports = resolvers;
