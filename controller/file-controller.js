const Grid = require("gridfs-stream");
const mongoose = require("mongoose");
const mongodb = require("mongodb");
const dotenv = require("dotenv");
dotenv.config();
eval(
  `Grid.prototype.findOne = ${Grid.prototype.findOne
    .toString()
    .replace("nextObject", "next")}`
);

let gfs;
const conn = mongoose.connection;
conn.once("open", function () {
  gfs = new Grid(conn.db, mongoose.mongo);
  gfs.collection("files");
});

exports.getFile = function (req, res) {
  gfs.findOne({ filename: req.params.filename }, function (err, file) {
    if (err) {
      return res.status(400).send({
        err: errorHandler.getErrorMessage(err),
      });
    }
    if (!file) {
      return res.status(404).send({
        err: "File not found.",
      });
    }

    if (req.headers["range"]) {
      const parts = req.headers["range"].replace(/bytes=/, "").split("-");
      const partialstart = parts[0];
      const partialend = parts[1];

      const start = parseInt(partialstart, 10);
      const end = partialend ? parseInt(partialend, 10) : file.length - 1;
      const chunksize = end - start + 1;

      res.writeHead(206, {
        "Accept-Ranges": "bytes",
        "Content-Length": chunksize,
        "Content-Range": "bytes " + start + "-" + end + "/" + file.length,
        "Content-Type": file.contentType,
      });

      gfs
        .createReadStream({
          _id: file._id,
          range: {
            startPos: start,
            endPos: end,
          },
        })
        .pipe(res);
    } else {
      res.header("Content-Length", file.length);
      res.header("Content-Type", file.contentType);

      gfs
        .createReadStream({
          _id: file._id,
        })
        .pipe(res);
    }
  });
};

/* GET file */
// exports.getFile = async (req, res, next) => {
//   try {
//     const file = await gfs.files.findOne({ filename: req.params.filename });
//     const readStream = gfs.createReadStream({ filename: file.filename });
//     readStream.pipe(res);
//   } catch (error) {
//     console.log(error);
//     res.status(500).json({ content: "Error occur" });
//   }
// };

/* POST file */
exports.postFile = async (req, res) => {
  if (req.file === undefined)
    return res.status(404).json({ content: "File not found" });

  return res.status(200).json(req.file);
};

/* DELETE file */
exports.deleteFile = async (req, res) => {
  try {
    await gfs.files.deleteOne({ filename: req.params.filename });
    res.status(200).json({ content: "File deleted" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ content: "Error occur" });
  }
};
