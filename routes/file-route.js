const express = require("express");
const router = express.Router();
const upload = require("../middleware/upload");
const fileController = require("../controller/file-controller");

/* GET file */
router.get("/:filename", fileController.getFile)

/* POST file */
router.post("/", upload.single("file"), fileController.postFile);

/* DELETE file */
router.delete("/:filename", fileController.deleteFile)

module.exports = router;