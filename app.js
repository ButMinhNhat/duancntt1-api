const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const cors = require("cors");
const { graphqlHTTP } = require("express-graphql");
const { ApolloServer } = require("apollo-server-express");
const { gql } = require("apollo-server-express");
const { graphqlUploadExpress } = require('graphql-upload');
const jwt = require("jsonwebtoken");
const {
  ApolloServerPluginLandingPageGraphQLPlayground,
} = require("apollo-server-core");
const _ = require("lodash");
const { buildSubgraphSchema } = require("@apollo/subgraph");
const { mergeTypeDefs, mergeResolvers } = require("@graphql-tools/merge");
const { loadFilesSync } = require('@graphql-tools/load-files')
const fileRouter = require('./routes/file-route');

// Load schema & resolvers
const typesArray      = loadFilesSync(path.join(__dirname, "./schema"));
const resolverssArray = loadFilesSync(path.join(__dirname, "./resolver"));

const app = express();
// const server = require("http").createServer(app);
const server = new ApolloServer({
  schema: buildSubgraphSchema({
    typeDefs: mergeTypeDefs(typesArray),
    resolvers: mergeResolvers(resolverssArray),
  }),
  context: ({ req }) => {
	const token = req.headers.authorization?.replace("Bearer ", "");
	if(token) {
	  const userId = jwt.decode(token)?.userId;
	  return { userId }
	};
  },
  introspection: false,
  plugins: [ApolloServerPluginLandingPageGraphQLPlayground()],
});

dotenv.config();

mongoose
  .connect(process.env.MONGO_HOST, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("DB connect"))
  .catch((err) => console.log(err));

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(graphqlUploadExpress());
app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// Add headers before the routes are defined
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  // Pass to next layer of middleware
  next();
});

app.use('/file', fileRouter);

// // catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   next(createError(404));
// });

// // error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });

//port listen
const PORT = process.env.PORT || 8080;
server.start().then(() => {
  server.applyMiddleware({ app });
  app.listen(PORT, () => {
    console.log(
      `🚀 Server is running on http://localhost:${PORT}${server.graphqlPath}`
    );
  });
});

module.exports = app;
