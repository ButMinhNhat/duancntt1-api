const jwt = require("jsonwebtoken");

function generateAccessToken(userId) {
  return jwt.sign(userId, process.env.TOKEN_SECRET, { expiresIn: "2592000s" });
}

function authenticateToken(req, res, next) {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];

  if (token == null) return res.sendStatus(401).send("unauthorization");

  jwt.verify(token, process.env.TOKEN_SECRET, (err, result) => {
    if (err) return res.sendStatus(403).send("forbidden");
	req.body.createdBy = result.userId;   
    next();
  });
}

module.exports = {
  generateAccessToken,
  authenticateToken
};
