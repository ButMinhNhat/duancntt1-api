const mongoose = require("mongoose");

const { Schema } = mongoose;

const accountSchema = new Schema({
	name: { type: String, default: "" },
	username: { type: String, required: true, unique: true },
	password: { type: String, required: true },
	avatar: { type: String, default: "" }
}, { timestamps: true });

const Account = mongoose.model('account', accountSchema);

module.exports = Account;