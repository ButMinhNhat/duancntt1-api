const mongoose = require("mongoose");

const { Schema } = mongoose;

const transactionSchema = new Schema({
	type: { type:String, require: true }, //course-rating, comment, quiz-answer, upvote
    course: { type: Schema.Types.ObjectId, ref: "course", require: true },
    parent: { type: Schema.Types.ObjectId }, // commentId, , quizId.
    content: { 
		value: { type: Number },
		text: { type: String },
		answers: [{
			text: { type: String },
			chooses: [{ type: String }]
		}],
		score: { type: Number }
	},
    createdBy: { type: Schema.Types.ObjectId, ref: "account" }
}, { timestamps: true });

const Transaction = mongoose.model('transaction', transactionSchema);

module.exports = Transaction;