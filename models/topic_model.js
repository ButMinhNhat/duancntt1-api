const mongoose = require("mongoose");

const { Schema } = mongoose;

const topicSchema = new Schema({
	slug: { type: String, required: true, unique: true },
	name: { type: String, required: true, unique: true },
	parent: { type: Schema.Types.ObjectId, ref: "topic" }
}, { timestamps: true });

const Topic = mongoose.model('topic', topicSchema);

module.exports = Topic;