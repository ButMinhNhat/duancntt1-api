const mongoose = require("mongoose");

const { Schema } = mongoose;

const trackingSchema = new Schema({
	type: { type:String, require: true }, 
    course: { type: Schema.Types.ObjectId, ref: "course", require: true },
    data: [{ type: Schema.Types.ObjectId }],
    createdBy: { type: Schema.Types.ObjectId, ref: "account" }
}, { timestamps: true });

const Tracking = mongoose.model('tracking', trackingSchema);

module.exports = Tracking;