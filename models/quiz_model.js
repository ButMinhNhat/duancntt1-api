const mongoose = require("mongoose");

const { Schema } = mongoose;

const question = new Schema({
    type: {type: String, default: 'TEXT'},
    title: {type: String, required: true},
    answers: [{type: String}],
    correctAnswers: [{type: String}],
})

const quiz = new Schema({
    title: {type: String},
    questions: [question],
    sequence: {type: Number},
}, { timestamps: true });

const Quiz = mongoose.model('quiz', quiz);

module.exports = Quiz;