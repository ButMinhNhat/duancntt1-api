const mongoose = require("mongoose");

const { Schema } = mongoose;

const courseSection = new Schema({
    title: {type: String, required: true},
    quiz:  [{type:  Schema.Types.ObjectId, ref: 'quiz'}],
    lectures: [{
        title: {type: String, required: true},
        media: {type: String},
        files: [{type: String}],
        content: {type: String},
        sequence: {type: Number}
    }]
})

const courseModel = new Schema({
	title: { type: String, required: true },
	slug: { type: String, required: true, unique: true },
	status: { type: String, default: 'private' },
	attendees: [{ type: Schema.Types.ObjectId, ref: "account" }],
	sections: [courseSection],
	shortDescription: { type: String },
	description: { type: String },
	coverImage: { type: String, default: "" },
	level: { type: String, default: "normal" },
	hashtags: [{type: String}],
	features: [{ type: String }],
	topic:  [{ type: Schema.Types.ObjectId, ref: "topic" }],
	createdBy: { type: Schema.Types.ObjectId, ref: "account" }
}, { timestamps: true });

const Course = mongoose.model('course', courseModel);

module.exports = Course;