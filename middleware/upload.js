const multer = require("multer");
const {GridFsStorage} = require('multer-gridfs-storage');
const dotenv = require("dotenv");
dotenv.config();

const storage = new GridFsStorage({
    url: process.env.MONGO_HOST,
    options: { useNewUrlParser: true, useUnifiedTopology: true },
    file: (req, file) => {
        // const match = ["image/png", "image/jpeg"];

        // if (match.indexOf(file.mimetype) === -1) {
        //     const filename = `${Date.now()}-${file.originalname}`;
        //     return filename;
        // }

        return {
            bucketName: "files",
            filename: `${Date.now()}-${file.originalname}`,
        };
    },
});

module.exports = multer({ storage });